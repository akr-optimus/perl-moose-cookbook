package ClassA;

use Moose;
use XML::LibXML;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str',
    );

has 'age' => (
    'is' => 'rw',
    'isa' => 'Int',
    );

has 'emails' => (
    'is' => 'rw',
    'isa' => 'ArrayRef[Str]',
    'default' => sub { [] },
    );

has 'vehicle' => (
    'is' => 'rw',
    'isa' => 'Bool',
    'default' => 0,
    'predicate' => 'has_vehicle',
    'clearer' => 'no_vehicle',
    'trigger' => \&_checkInsurance,
    );

has 'insurance' => (
    'is' => 'rw',
    'isa' => 'Int',
    'default' => 0,
    );

has 'detailsFile' => (
    'is' => 'rw',
    'isa' => 'Str',
    'predicate' => 'has_details',
    'clearer' => 'no_details'
    );

has 'detailsDoc' => (
    'is' => 'ro',
    'isa' => 'XML::LibXML::Document',
    'builder' => 'setup_details',
    );

sub addEmail {
    my $self = shift;
    my @email = @_;
    push @{ $self->{emails} }, @email;
}

sub setup_details {
    my $self = shift;
    my $xml = new XML::LibXML();
    # return $xml->parse_file($self->detailsFile());
    return $xml->parse_file("/tmp/details.xml");
}

sub _checkInsurance {
    my $self = shift;
    my ($veh, $old_veh) = @_;
    if ($veh && !$old_veh) {
        warn "You need an Insurance!\n" if (!$self->insurance());
        $self->insurance(2000);
    } elsif (!$veh && $old_veh) {
        warn "You need to cancel your insurance.\n" if ($self->insurance());
    }
}
    
1;
