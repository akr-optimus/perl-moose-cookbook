package ClassA;

use Moose;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str',
    );

around BUILDARGS => sub {
    my $method = shift;
    my $class = shift;

    if (@_ == 0) {
        return $class->$method(name => "NoName");
    } elsif (@_ == 1 && ref($_[0]) ne 'HASH') {
        return $class->$method(name => $_[0]);
    } elsif (@_ == 2 && ref($_[0]) ne 'HASH') {
        return $class->$method(@_);
    } elsif (@_ == 1 && ref($_[0]) eq 'HASH') {
        return $class->$method(@_);
    }
};

# sub BUILD {
#     my $self = shift;
#     $self->name(uc $self->name());
# }

    
1;
