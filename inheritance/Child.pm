package Child;

use Moose;

extends 'Parent';

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str'
    );

has 'team' => (
    'is' => 'rw',
    'isa' => 'Str'
    );


1;
