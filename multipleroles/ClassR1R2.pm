package ClassR1R2;

use Moose;

with 'Role1', 'Role2';

has 'name' => (
    'is' => 'rw',
    'isa' => 'Int',
    'default' => 0,
    );

1;
