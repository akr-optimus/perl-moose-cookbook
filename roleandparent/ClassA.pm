package ClassA;

use Moose;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str',
    );

sub myname {
    my $self = shift;
    return "My" . $self->name() . "says: " . $self->role();
}

1;
