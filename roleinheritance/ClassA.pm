package ClassA;

use Moose;
with 'Role2';

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str',
    );

sub what {
    my $self = shift;
    print $self->role() . " and " . $self->action() . "\n";
}

1;
