#!/usr/bin/perl
use warnings;
use strict;
use ClassA;

my $o = ClassA->new();
print $o->time(), "\n";
sleep 5;
my $p = ClassA->new();
print $p->time(), "\n";
